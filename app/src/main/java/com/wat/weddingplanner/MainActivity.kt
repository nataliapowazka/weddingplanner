package com.wat.weddingplanner

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button: Button = findViewById(R.id.button)
        button.setOnClickListener {
            findViewById<TextView>(R.id.textView).text = "Button 1"
        }

        val button2: Button = findViewById(R.id.button2)
        button.setOnClickListener {
            findViewById<TextView>(R.id.textView).text = "Button 2"
        }

    }
}